/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package exemple4;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * En aquest exemple es creen multiples fils que escriuen de forma simultania al mateix fitxer, sense cap sincronització.
 * Un cop tots els fils acaben el seu treball, finalitza l'aplicació.
 * Obrint el fitxer es pot observar que sense cap sincronització entre fils, els resultats són inconsistents.
 * 
 * @author manel
 */
public class M09uf1_Exemple4e {
    
    // classe que implementa la interfície Runnable i que ens permet tractar-la com a un fil
    static Runnable job;
    
    // s'utilitza per "inventar" tasques
    static Random rand = new Random();
    
    //llista on desarem els threads a mida que es vagi creant
    static List<Thread> llistaThreads;
    
    // nombre de fils a crear
    static int nombreFils = 20;
    
    public static void main(String[] args) {
        
        llistaThreads = new ArrayList<>();
        
        System.out.println("M09uf1_Exemple4e");
        System.out.println("================");
        System.out.println();
        
        try {          
            
            File fitxer = File.createTempFile("M09uf1_Exemple4e_", ".tmp");           
            
            System.out.println("Creant "+ nombreFils + " fils...");
            for (int i = 0; i < nombreFils; i++)
            {
                //objecte job
                job = new WriteBytes(fitxer.getAbsolutePath(), rand.nextInt(5,10), true);
                
                //prepara l'execució de la job en un fil independent
                Thread fil = new Thread(job);
                
                //iniciem la job
                fil.start();
                
                //l'afegim a la llista per al seu posterior control
                llistaThreads.add(fil);
            }
            
            //esperem a que tots els fils estiguin finalitzats
            for (Thread t : llistaThreads)
                t.join();
            
            System.out.println("Resultats al fixter: " + fitxer.getAbsolutePath());
            
            //finalitzem l'aplicació
            System.exit(0);

        } catch (IOException | InterruptedException e) {
            System.out.println("ERROR: " + e.toString());
        }
        
    }
}

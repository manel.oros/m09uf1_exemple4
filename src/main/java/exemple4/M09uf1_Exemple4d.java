/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package exemple4;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * En aquest exemple es crea una sola tasca que realitza un compte enrere i més d'un fil ha de competir per a realitzar-la correctament.
 * Hi ha dos tipus de tasca, una thread safe i l'altre no.
 * 
 * Un cop acaben tots els fils, finalitza l'aplicació.
 * 
 * @author manel
 */
public class M09uf1_Exemple4d {
    
    // classe que implementa la interfície Runnable i que ens permet tractar-la com a un fil
    static Runnable jobNothreadSafe, jobThreadSafe;
    
    // s'utilitza per "inventar" tasques
    static Random rand = new Random();
    
    //llista on desarem els threads a mida que es vagi creant
    static List<Thread> llistaThreads;
    
    // nombre de fils a crear
    static int nombreFils = 2;
    
    
    public static void main(String[] args) {
        
        System.out.println("M09uf1_Exemple4d");
        System.out.println("================");
        System.out.println();
        
        Integer cicles = rand.nextInt(10,15);
        Integer milisegons = rand.nextInt(1000,2000);
        
         //tasques a executar
         jobNothreadSafe = new CountDown_NothreadSafe(cicles, milisegons, true);
       
        
        try {          
            
            System.out.println();
            System.out.println("Creant "+ nombreFils + " fils per compte enreere No ThreadSafe");
            
            //reiniciem llista threads
            llistaThreads = new ArrayList<>();
            
            for (int i = 0; i < nombreFils; i++)
            { 
                //habilita l'execució de la job com a fil
                Thread fil = new Thread(jobNothreadSafe);
                
                //iniciem el fil
                fil.start();
                
                //l'afegim a la llista per al seu posterior control
                llistaThreads.add(fil);
            }
            
            //esperem a que tots els fils estiguin finalitzats
            for (Thread t : llistaThreads)
                t.join();
            
            System.out.println();
            System.out.println("Creant "+ nombreFils + " fils per compte enreere ThreadSafe");
            jobThreadSafe = new CountDown_ThreadSafe(cicles, milisegons, true);
            
            //reiniciem llista threads
            llistaThreads = new ArrayList<>();
            
            for (int i = 0; i < nombreFils; i++)
            { 
                //habilita l'execució de la job com a fil
                Thread fil = new Thread(jobThreadSafe);
                
                //iniciem el fil
                fil.start();
                
                //l'afegim a la llista per al seu posterior control
                llistaThreads.add(fil);
            }
            
            //esperem a que tots els fils estiguin finalitzats
            for (Thread t : llistaThreads)
                t.join();
            
            //finalitzem l'aplicació
            System.exit(0);

        } catch (Exception e) {
            System.out.println("ERROR: " + e.toString());
        }
        
    }
}

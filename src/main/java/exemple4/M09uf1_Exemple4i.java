/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package exemple4;

import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Deadlock
 * Dos recursos i dos fils. Cada fil vol bloquejar els dos recursos alhora.
 * Queden indefinidament esperant.
 * 
 * @author manel
 */
public class M09uf1_Exemple4i {
    
    // objectes a monitorar o bloquejar
    static ReentrantLock lock1 = new ReentrantLock();
    static ReentrantLock lock2 = new ReentrantLock();
    
    public static void main(String[] args) {
        
        Thread t1 = new Thread(){
            
            @Override
            public void run(){

                lock1.lock();

                    System.out.println("Thread 1: Blocada zona 1");

                    try { Thread.sleep(10); } catch (InterruptedException e) {}

                    System.out.println("Thread 1: Esperant per blocar zona 2...");
                    lock2.lock();

                            System.out.println("Thread 1: Blocades zona 1 i 2.");

                    lock2.unlock();

                lock1.unlock();
            }
        };
        
        Thread t2 = new Thread(){

            @Override
            public void run(){

                lock2.lock();

                    System.out.println("Thread 2: Blocada zona 2");

                    try { Thread.sleep(10); } catch (InterruptedException e) {}

                    System.out.println("Thread 2: Esperant per blocar zona 1...");
                    lock1.lock();

                        System.out.println("Thread 2: Blocades zona 1 i 2.");

                    lock1.unlock();

                lock2.unlock();
            }
        };
         
        // iniciem fils
        t1.start();
        t2.start();
        
        //esperem que acabin els fils per tancar l'aplicació
        try {
            t1.join();
            t2.join();
        } catch (InterruptedException ex) {
            Logger.getLogger(M09uf1_Exemple4i.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        //finalitzem l'aplicació
        System.exit(0);
    }
}

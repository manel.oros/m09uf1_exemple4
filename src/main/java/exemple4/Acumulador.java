/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package exemple4;

import java.util.ArrayList;

/**
 * Classe que, donada una llista de nombres enters, afegeix n nombres al final de la llista.
 * Cada nombre afegit és una unitat superior a l'alterior.
 * 
 * Exemple: 
 * - Llista inicial: 10,11,12,13,14,15
 * - Iteracions = 10
 * - Llista final: 10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25
 * @author manel
 */
public class Acumulador implements Runnable {
    
    private final ArrayList<Integer> llistaNombres;
    private ArrayList<Integer> objecteASincronitzar = new ArrayList<>();
    Integer iteracions;
    
    

    /***
     * Constructor
     * @param llistaNombres: llista comú on acumular nombres
     * @param iteracions: nombres a acumular
     * @param threadSafe: si true, sincronitza la llista 
     */
    public Acumulador(ArrayList<Integer> llistaNombres, Integer iteracions, boolean threadSafe) {
        this.llistaNombres = llistaNombres;
        if (threadSafe){
            this.objecteASincronitzar = this.llistaNombres;
        }
        this.iteracions = iteracions;
    }
    
    @Override
    public void run() {
        
        for (int i = 0; i < iteracions; i++){
           synchronized(objecteASincronitzar){
                try
                {
                    if (this.llistaNombres.size() == 0)
                        this.llistaNombres.add(1);
                    else
                    {
                        int size = this.llistaNombres.size();
                        int n = this.llistaNombres.get(size-1);
                        this.llistaNombres.add(n+1);
                    }
                } catch (Exception ex){
                     System.out.println("Error en la iteració!!");
                }
           }
        }
    }
}

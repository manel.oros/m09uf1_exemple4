/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package exemple4;

import static exemple4.Common.infoThread;
import java.util.UUID;
import java.util.concurrent.locks.ReentrantLock;

/**
 *
 * @author manel
 */
/***
     * Classe que simula una tasca a realitzar.
     * Implementa runnable i per tant és executable mitjançant fils
     * Aquesta tasca és Thread-safe. Això vol dir que si és accedida per més d'un fil, aquests ho fan de forma sincronitzada.
     * 
     */
    public class CountDown_ThreadSafe implements Runnable{
        
        ReentrantLock lock = new ReentrantLock();
        
        //atribut que defineix si s'imprimeix status
        boolean printStatus;
        
         //cicles de duració
        int cicles;
        
        //temps que dura cada cicle
        int milisegons;
        
        String id;
        
        /***
         * Tasca amb una serie de cicles i uns determinats milisegons per cicle
         * @param _cicles
         * @param _milisegons 
         * @param _printStatus 
         */
        public CountDown_ThreadSafe(int _cicles, int _milisegons, boolean _printStatus) {
            this.cicles = _cicles;
            this.milisegons = _milisegons;
            this.printStatus = _printStatus;
            this.Init();
        }
        
        private void Init()
        {
            this.id = UUID.randomUUID().toString();
            System.out.println("Generant compte enrere des de " + cicles + " i amb " + milisegons + " milisegons per iteració");
        }
        
        @Override
        public void run(){
            
        //inici de la zona de bloqueig. A partir d'aquí, solament pot entrar un fil alhora.
        lock.lock();

        try {

            //tasca de cicles determinada
            while(cicles > 0){
                
                Thread.sleep(milisegons);

                if (printStatus)
                     System.out.println("[" + infoThread(Thread.currentThread(),true) + "]" + this.cicles + "...");

                this.cicles--;
            }

            } catch (InterruptedException ex) {
                System.out.println("Error en l'execució del fil: " + ex.toString());
            } finally{
               //fi de la zona de bloqueig
               lock.unlock();
            }

          System.out.println("[" + infoThread(Thread.currentThread(),true) + "] finalitzat");

        }
    }

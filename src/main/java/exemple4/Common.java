/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package exemple4;

/**
 * Classe d'utilitat amb mètodes comuns a tots els exemples
 * 
 * @author manel
 */
public class Common {
    
    /***
     * Classe que imprimeix informació sobre un determinat fil
     * 
     * @param t Thread
     * @param isBrief si true, versió curta
     * @return 
     */
    public static String infoThread(Thread t, boolean isBrief)
    {
        //inicialitzem variable de retorn
        String ret = "";
        
        if (isBrief)
        {
            ret += "Thread id:" + t.getId();
            
        }else {
            
            ret += "ID:          " + t.getId() + System.lineSeparator();
            ret += "Nom:         " + t.getName() + System.lineSeparator();
            ret += "Prioritat:   " + t.getPriority() + System.lineSeparator();
            ret += "Estat:       " + t.getState() + System.lineSeparator();
            ret += "Actiu:       " + t.isAlive() + System.lineSeparator();
            ret += "Daemon:      " + t.isDaemon() + System.lineSeparator();
            ret += "Interromput: " + t.isInterrupted() + System.lineSeparator();
        }
        
        // retornem
        return ret;
        
    }
    
}
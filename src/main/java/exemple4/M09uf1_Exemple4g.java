/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package exemple4;

/**
 * Exemple de comunicació i sincronització entre diferents fils amb wait() i notifyAll().
 * L'espai comú de la comunicació entre fils es simula mitjançant una instància de la classe anomenada SharedMemo
 * 
 * 
 * (1) - Un tercer fil emissor de missatges, emet un missatge i desbloqueja els receptors amb notifyAll()
 * (2) - Els receptors llavors reben el missatge i el mostren
 * (3) - El fil principal espera a que finalitzin els tres fils i acaba
 *  
 * @author manel
 */
public class M09uf1_Exemple4g {
    
    public static void main(String[] args) {
        
        System.out.println("M09uf1_Exemple4g");
        System.out.println("================");
        System.out.println();
        
        SharedMemo data = new SharedMemo();

        try {
            // (1)
            SynchronizedReceiver receiver = new SynchronizedReceiver(data);
            Thread receiverThread = new Thread(receiver, "receiver1-thread");
            receiverThread.start();
            
            SynchronizedReceiver receiver2 = new SynchronizedReceiver(data);
            Thread receiverThread2 = new Thread(receiver2, "receiver2-thread");
            receiverThread2.start();
            
            Thread.sleep(5000);

            //(2)
            SynchronizedSender sender = new SynchronizedSender("Hola Pepe!!", data);
            Thread senderThread = new Thread(sender, "sender-thread");
            senderThread.start();
            
            //(3)
            receiverThread.join();
            receiverThread2.join();
            senderThread.join();
            
        } catch (InterruptedException interruptedException) {
        }
        
        //finalitzem l'aplicació
        System.exit(0);
    }
    
    /***
     * Classe de tipus "Nested class". 
     * Es considera d'aquest tipus per dos motius:
     * - És declarada dintre d'un altre classe
     * - És declarada de tipus static
     * Una classe d'aquest tipus sempre és accessible des de la classe contenidora.
     * (si no es declara static, llavors és una INNER CLASS)
     */
    private static class SharedMemo {
        
        private String message;

        public void send(String message) {
            this.message = message;
        }

        public String receive() {
            return message;
        }
    }
    
    /***
     * Classe que emet un missatge
     */
    public static class SynchronizedSender implements Runnable {
        private final SharedMemo data;
        private final String message;
        

        public SynchronizedSender(String message, SharedMemo data) {
            this.data = data;
            this.message = message;
        }

        @Override
        public void run() {
            synchronized (data) {
                System.out.println(Common.infoThread(Thread.currentThread(), true)+ " Enviant missatge: " + this.message);
                data.send(this.message);
                System.out.println(Common.infoThread(Thread.currentThread(), true)+ " Notificant a receptors...");
                data.notify(); // allibera el primer thread
                data.notify(); //allibera el segon thread
                
                //també equivalent a fer notifyAll()
            }
        }
    }
    
    /***
     * Classe que espera fins que es rep una senyal notifyall i llavors mostra el missatge rebut
     */
    public static class SynchronizedReceiver implements Runnable {
        private final SharedMemo data;
        private String message;

        
        public SynchronizedReceiver(SharedMemo data) {
            this.data = data;
        }

        @Override
        public void run() {
            synchronized (data) {
                try {
                    System.out.println(Common.infoThread(Thread.currentThread(), true) + " Esperant missatge...");
                    data.wait();
                    this.message = data.receive();
                    System.out.println(Common.infoThread(Thread.currentThread(), true)+ " Missatge rebut: " + this.message);
                } catch (InterruptedException e) {
                    System.out.println("thread was interrupted: " + e);
                    Thread.currentThread().interrupt();
                }
            }
        }

        public String getMessage() {
            return message;
        }
    }
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package exemple4;

import java.util.ArrayList;
import java.util.List;

/**
 * En aquest exemple multiples fils han de colaborar per acumular un nombre fix d'elements a una col.lecció,
 * on cada element ha de tenir un valor basat en l'element anterior.
 * Sense sincronització és impossible realitzar la tasca
 * 
 * 
 * 
 * @author manel
 */
public class M09uf1_Exemple4f {
    
    static ArrayList<Integer> nombres;
    
    // classe que implementa la interfície Runnable i que ens permet tractar-la com a un fil
    static Runnable job;
    
    //llista on desarem els threads a mida que es vagi creant
    static List<Thread> llistaThreads;
    
    public static void main(String[] args) {
        // nombre de fils a crear
        int nombreFils = 20;
        int nombreIteracions = 100;
        
        llistaThreads = new ArrayList<>();
        
        System.out.println("M09uf1_Exemple4f");
        System.out.println("================");
        System.out.println();
        
        System.out.println("Sense sincronització:");
        System.out.println("---------------------");
        nombres = new ArrayList<>();
        bucle (nombreFils, nombreIteracions, false);
        
        System.out.println();
        System.out.println("Amb sincronització:");
        System.out.println("---------------------");
        nombres = new ArrayList<>();
        bucle (nombreFils, nombreIteracions, true);
        
        //finalitzem l'aplicació
        System.exit(0);
    }
    
    private static void bucle(Integer nfils, Integer iteracions, boolean threadSafe)
    {
        try {
            System.out.println("Creant "+ nfils + " fils amb " + iteracions + " cicles per fil = " + nfils * iteracions + " elements previstos a la llista" );
            for (int i = 0; i < nfils; i++)
            {
                //objecte job
                job = new Acumulador(nombres, iteracions, threadSafe);
                
                //prepara l'execució de la job en un fil independent
                Thread fil = new Thread(job);
                
                //iniciem la job
                fil.start();
                
                //l'afegim a la llista per al seu posterior control
                llistaThreads.add(fil);
            }
            
            //esperem a que tots els fils estiguin finalitzats
            for (Thread t : llistaThreads)
                t.join();
            
            System.out.println("Elements trobats a la llista: " + (nombres.size()));
            System.out.println("Valor del darrer element trobat a la llista: " + nombres.get(nombres.size()-1));
            //nombres.stream().forEach(x -> System.out.print(x+";"));
        } catch (InterruptedException ex) {
            System.out.println("ERROR: " + ex.toString());
        }
    }
}

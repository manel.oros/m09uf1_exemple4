/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package exemple4;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * En aquest exemple es creen multiples tasques de compte enreere.
 * Cada tasca té el seu propi fil.
 * En cap moment hi ha interferència ni comunicació entre els fils. Un cop acaben tots els fils, finalitza l'aplicació.
 * 
 * @author manel
 */
public class M09uf1_Exemple4c {
    
    // classe que implementa la interfície Runnable i que ens permet tractar-la com a un fil
    static Runnable job;
    
    // s'utilitza per "inventar" el treball
    static Random rand = new Random();
    
    //llista on desarem els threads a mida que es vagi creant
    static List<Thread> llistaThreads;
    
    // nombre de fils a crear
    static int nombreFils = 5;
    
    
    public static void main(String[] args) {
        
        llistaThreads = new ArrayList<>();
        
        System.out.println("M09uf1_Exemple4c");
        System.out.println("================");
        System.out.println();
        
        try {          
            
            System.out.println("Creant "+ nombreFils + " fils...");
            for (int i = 0; i < nombreFils; i++)
            {
                //realitza la job a executar en un fil
                job = new CountDown_NothreadSafe(rand.nextInt(3,6), rand.nextInt(1000,2000), true);
                
                //habilita l'execució de la job com a fil
                Thread fil = new Thread(job);
                
                //iniciem el fil
                fil.start();
                
                //l'afegim a la llista per al seu posterior control
                llistaThreads.add(fil);
            }
            
            //esperem a que tots els fils estiguin finalitzats
            for (Thread t : llistaThreads)
                t.join();
            
            //finalitzem l'aplicació
            System.exit(0);

        } catch (Exception e) {
            System.out.println("ERROR: " + e.toString());
        }
        
    }
}

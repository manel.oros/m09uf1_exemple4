/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package exemple4;

import static exemple4.Common.infoThread;
import java.util.Random;

/**
 * En aquest exemple es crea un fil que fa un treball finit.
 * L'aplicació espera a que acabi el fil. Un cop acaba el fil, finalitza l'aplicació.
 * 
 * 
 * @author manel
 */
public class M09uf1_Exemple4b {
    
    // s'utilitza per "inventar" tasques
    static Random rand = new Random();
    
    // classe que implementa la interfície Runnable i que ens permet tractar-la com a un fil
    static Runnable job;

    public static void main(String[] args) {
        
        System.out.println("M09uf1_Exemple4b");
        System.out.println("================");
        System.out.println();
        
        try {          

            //realitza la job a executar en un fil
            job = new CountDown_NothreadSafe(10, rand.nextInt(1000,2000), true);

            //habilita l'execució de la job com a fil
            Thread fil = new Thread(job);

            //iniciem el fil
            fil.start();
            
            System.out.println("Fil creat correctament");
            
            //imprimim informació
            System.out.println(infoThread(fil, false));
            
            System.out.println("Esperant a que finalitzi el fil");
            
            //aquest mètode pausa l'aplicació fins que el fil acaba
            fil.join();
            
            //finalitzem l'aplicació
            System.exit(0);

        } catch (Exception e) {
            System.out.println("ERROR: " + e.toString());
        }
        
    }
}

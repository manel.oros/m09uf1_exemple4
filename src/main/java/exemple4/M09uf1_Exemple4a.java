/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package exemple4;


import static exemple4.Common.infoThread;
import java.util.Random;

/**
 * En aquest exemple es crea un fil que fa un treball infinit.
 * Quan l'aplicació finalitza, el fil mor també.
 * 
 * 
 * @author manel
 */
public class M09uf1_Exemple4a {
    
    // s'utilitza per "inventar" tasques
    static Random rand = new Random();
        
    
    public static void main(String[] args) {
        
        // classe que implementa la interfície Runnable i que ens permet tractar-la com a un fil
        Runnable job;
        
        
        // el que ha de durar la nostra aplicació
        int segons_fins_finalitzar = 20;
        
        System.out.println("M09uf1_Exemple4a");
        System.out.println("================");
        System.out.println();
        
        try {

            System.out.println("Creant el fil...");
            
            //realitza la tasca a executar en un fil
            job = new CountDown_NothreadSafe(Integer.MAX_VALUE, rand.nextInt(1000,2000), true);

            //habilita l'execució de la tasca com a fil
            Thread fil = new Thread(job);

            //iniciem el fil
            fil.start();
            
            System.out.println("Fil creat correctament");
            
            //imprimim informació
            System.out.println(infoThread(fil, false));
            
            System.out.println("Esperant " + segons_fins_finalitzar + " segons fins a finalitzar l'aplicació");
            
            // parem l'aplicació un temps (fil principal)
            Thread.sleep(segons_fins_finalitzar*1000);
            
            System.out.println("Finalitzant aplicació...");
            System.exit(0);

        } catch (Exception e) {
            System.out.println("ERROR: " + e.toString());
        }
    }
    
    
}

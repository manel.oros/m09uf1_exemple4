/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package exemple4;

import static exemple4.Common.infoThread;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.concurrent.locks.ReentrantLock;

/**
 *
 * @author manel
 */
/***
     * Classe que genera una cadena de caracters i l'escriu en un fitxer.
     * /
     * Implementa runnable i per tant és convertible en un fil.
     * 
     */
    public class WriteBytes implements Runnable{
        
        //static ReentrantLock lock = new ReentrantLock();
        
        //atribut que defineix si s'imprimeix status
        boolean printStatus;
        
        //cicles de duració
        int cicles;
       
        // Fitxer de sortida
        String path;

        /***
         * Tasca amb una serie de cicles i uns determinats milisegons per cicle
         * @param f
         * @param _cicles
         * @param _printStatus 
         */
        public WriteBytes(String f, int _cicles, boolean _printStatus) {
            this.cicles = _cicles;
            this.printStatus = _printStatus;
            this.path = f;
        }
        
        @Override
        public void run(){
            
            System.out.println("Fil: [" + infoThread(Thread.currentThread(),true) + "] Escribint " + cicles + " linies al fixter " + this.path );

            //tasca de cicles determinada
            while(cicles > 0){

                String toWrite =  "ABCDEFGHIJKLMNOPQRSTUVWXYZ" + System.lineSeparator();
                
                writeBytes(this.path, toWrite);

                if (printStatus)
                    System.out.println(toWrite);

                this.cicles--;

            }
        }
        
        /***
         * Mètode estàtic que escriu, byte a byte, una cadena en un fitxer
         * @param _path Ruta al fitxer
         * @param str Cadena a escriure
         * 
         * Amb "private synchronized void writeBytes(String _path, String str)" 
         * també aconseguim el mateix efecte que amb lock
         * 
         */
        private static void writeBytes(String _path, String str)
        {
            
            FileOutputStream outputStream = null;
                    
            try {
                
                outputStream = new FileOutputStream(_path, true);
                
                //lock.lock();
                
                for (int i = 0; i<str.length(); i++)
                {
                    byte b = (byte)str.charAt(i);
                    
                    outputStream.write(b);
                }
                
                outputStream.close();
                
            } catch (FileNotFoundException ex) {
                System.out.println(ex);
            } catch (IOException ex) {
                System.out.println(ex);
            } finally {
                //lock.unlock();
                try {
                    outputStream.close();
                } catch (IOException ex) {
                    System.out.println(ex);
                }
            }
        }
    }

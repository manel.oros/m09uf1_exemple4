/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package exemple4;

import static exemple4.Common.infoThread;
import java.util.UUID;

/**
 *
 * @author manel
 */
/***
     * Classe que simula una tasca a realitzar.
     * Implementa runnable i per tant és executable mitjançant fils
     * Aquesta tasca no és Thread-safe. Això vol dir que si és accedida per més d'un fil, no hi ha cap tipus de sincronització
     * i els resultats poden ser no consistents.
     * 
     */
    public class CountDown_NothreadSafe implements Runnable{
        
        //atribut que defineix si s'imprimeix status
        boolean printStatus;
        
         //cicles de duració
        int cicles;
        
        //temps que dura cada cicle
        int milisegons;
        
        String id;
        
        /***
         * Tasca amb una serie de cicles i uns determinats milisegons per cicle
         * @param _cicles
         * @param _milisegons 
         * @param _printStatus 
         */
        public CountDown_NothreadSafe(int _cicles, int _milisegons, boolean _printStatus) {
            this.cicles = _cicles;
            this.milisegons = _milisegons;
            this.printStatus = _printStatus;
            this.Init();
        }
        
        private void Init()
        {
            this.id = UUID.randomUUID().toString();
            System.out.println("Generant compte enrere des de " + cicles + " i amb " + milisegons + " milisegons per iteració");
        }
        
        @Override
        public void run(){
            
            //tasca de cicles determinada
            while(cicles > 0){
                 
                try {
                    Thread.sleep(milisegons);
                    
                    if (printStatus)
                        System.out.println("[" + infoThread(Thread.currentThread(),true) + "]" + this.cicles + "...");
                    
                    this.cicles--;
                    
                } catch (InterruptedException ex) {
                    System.out.println("Error en l'execució del fil: " + ex.toString());
                }
            }
            
            System.out.println("[" + infoThread(Thread.currentThread(),true) + "] finalitzat");
        }
    }

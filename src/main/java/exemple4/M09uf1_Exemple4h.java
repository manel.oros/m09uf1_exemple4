/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package exemple4;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * En aquest exemple multiples fils passen un sol cop per una variable de tipus enter, utilitzada com acumuladror.
 * Es realitza una comparativa de fiabilitat dels tipus de variables que gestionen la concurrència en nombres enters
 * 
 * @author manel
 */
public class M09uf1_Exemple4h {
    
    static int acumuladorInt;
    static Integer acumuladorInteger;
    static volatile int acumuladorVolatileInt;
    static volatile Integer acumuladorVolatileInteger;
    static AtomicInteger acumuladorAtomicInteger;
    static volatile AtomicInteger acumuladorVolatileAtomicInteger;
    
    
    //llista on desarem els threads a mida que es vagi creant
    static List<Thread> llistaThreads;
    
    public static void main(String[] args) {
        // nombre de fils a crear
        int nombreFils = 100000;
        int nombreIteracions = 1;
        
        llistaThreads = new ArrayList<>();
        
        System.out.println("M09uf1_Exemple4h");
        System.out.println("================");
        System.out.println();
        
        System.out.println("static int acumuladorInt");
        System.out.println("-------------------------");
        System.out.println("Creant "+ nombreFils + " fils...");
        acumuladorInt=0;
        bucle (nombreFils, nombreIteracions, 0);
        System.out.println("Acumulador: " + acumuladorInt);
        System.out.println("");
        
        System.out.println("static Integer acumuladorInteger");
        System.out.println("--------------------------------");
        System.out.println("Creant "+ nombreFils + " fils...");
        acumuladorInteger=0;
        bucle (nombreFils, nombreIteracions, 1);
        System.out.println("Acumulador: " + acumuladorInteger);
        System.out.println("");
        
        System.out.println("static volatile int acumuladorVolatileInt");
        System.out.println("-----------------------------------------");
        System.out.println("Creant "+ nombreFils + " fils...");
        acumuladorVolatileInt=0;
        bucle (nombreFils, nombreIteracions, 2);
        System.out.println("Acumulador: " + acumuladorVolatileInt);
        System.out.println("");
        
        System.out.println("static volatile Integer acumuladorVolatileInteger");
        System.out.println("-------------------------------------------------");
        System.out.println("Creant "+ nombreFils + " fils...");
        acumuladorVolatileInteger=0;
        bucle (nombreFils, nombreIteracions, 3);
        System.out.println("Acumulador: " + acumuladorVolatileInteger);
        System.out.println("");
        
        System.out.println("static AtomicInteger acumuladorAtomicInteger");
        System.out.println("-----------------------------------------------------");
        System.out.println("Creant "+ nombreFils + " fils...");
        acumuladorAtomicInteger=new AtomicInteger(0);
        bucle (nombreFils, nombreIteracions, 4);
        System.out.println("Acumulador: " + acumuladorAtomicInteger);
        System.out.println("");
        
        System.out.println("static volatile AtomicInteger acumuladorVolatileAtomicInteger");
        System.out.println("------------------------------------------------------------");
        System.out.println("Creant "+ nombreFils + " fils...");
        acumuladorVolatileAtomicInteger=new AtomicInteger(0);
        bucle (nombreFils, nombreIteracions, 5);
        System.out.println("Acumulador: " + acumuladorVolatileAtomicInteger);
        System.out.println("");
        
       
        
        //finalitzem l'aplicació
        System.exit(0);
    }
    
    /***
     * Crea n fils i per a cada fil executa un sol cop el bloc de codi corresponent
     * @param nfils
     * @param iteracions
     * @param mode en funció del tipus de variable a utilitzar
     */
    private static void bucle(Integer nfils, Integer iteracions, int mode)
    {
        for (int i = 0; i < nfils; i++)
        {
            Thread t = new Thread(){
                
                @Override
                public synchronized void run(){
                    
                    switch (mode)
                    {
                        case 0 -> acumuladorInt++;
                        case 1 -> acumuladorInteger++;
                        case 2 -> acumuladorVolatileInt++;
                        case 3 -> acumuladorVolatileInteger++;
                        case 4 -> acumuladorAtomicInteger.incrementAndGet();
                        case 5 -> acumuladorVolatileAtomicInteger.incrementAndGet();
                    }
                }
            };
            llistaThreads.add(t);
            t.start();
        }
        
        //esperem a que tots els fils estiguin finalitzats
          for (Thread t : llistaThreads)
              try {
                  t.join();
            } catch (InterruptedException ex) {
                  System.out.println("Error amb el join: " + ex);
            }
    }
}
